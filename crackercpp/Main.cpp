
#include <string>
#include <fstream>
#include <set>
#include <algorithm>
#include <vector>
#include <chrono>
#include <mutex>

#include "BruteForcer.h"
#include "md5.h"

const void displayMenu() {
	printf("cracker: A program to hash passwords and cracks passwords, through bruteforce.\n\n");
	printf("Usage:\n");
    printf("cracker <file of hashes> <length of password(>=3)>\n");
}

// splits the file path into its extension and the rest
std::pair<std::string, std::string> splitFilePath(const std::string filePath) {
	size_t index = filePath.find_last_of(".");
	return std::pair<std::string, std::string>(filePath.substr(0, index), filePath.substr(index, filePath.length()));
}

// code to measure time taken
std::chrono::high_resolution_clock::time_point start;

double getTimeElapsed()
{
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    return duration / 1000000000.0;
}

// mutex to manipulate the unsolved password list
std::mutex mutex;

// Main.cpp : Starting point of the program.
int main(int argc, char* argv[])
{
    if(argc == 3) {
        
        int n = 0;
        // Catch some errors in the integer arg n
        try {
            n = std::stoi(argv[2]);
        } catch(...) {
            printf("Invalid number, please try again.\n");
            displayMenu();
			return EXIT_FAILURE;
        }
        if (n<3) {
            n = 3;
            printf("The program will just run for length 3.\n");
        }
        
        // catch some errors for the hashes file
		// temp for file read
		std::string eaLine;
		std::ifstream pwdfilestream(argv[1]);
		if (pwdfilestream.fail()) {
			printf("Cannot open hashes(%s), please ensure that the file exist.", argv[1]);
			displayMenu();
			return EXIT_FAILURE;
		}
		
		// Finished input validation, now to init
        // init lPow
        lPow[0] = 1;
        for(int x = 1; x<=(n+1); x++) {
            lPow[x] = lPow[x-1] * BASE;
        }

        // Store the file into appropriate containers
		std::vector<PasswordEntry> solved;
        std::set<Hash> unsolved;
		while (std::getline(pwdfilestream, eaLine))
		{
			unsolved.emplace(Hash(eaLine));
		}
        solved.reserve(unsolved.size());
		
		// just some local vars to make life easier
		const char l = MIN;
		const char u = MAX;
        
        // start the clock
		start = std::chrono::high_resolution_clock::now();
        
        // run length from 1 to 3 inclusive
        for(int d=1; d < 4; d++ ) {
            std::vector<PasswordEntry> currSolved = BruteForcer(0, lPow[d]-1, d).crack(unsolved);
            solved.insert(solved.end(), currSolved.begin(), currSolved.end());
            // update the unsolved
            for(int p=0; p<currSolved.size(); p++) {
                unsolved.erase(currSolved[p].hash);
            }
        }
        
        // for every length larger than or eq to 4, we use parallelism
        if( n >= 4 ) {
        
            // for any length larger or eq to 4
            for(int d = 4; d<= n;d++) {
            
                // define how small a piece each thread is going to solve
                ULLI delta = lPow[d-1];
                ULLI upper = lPow[d]-1;
                
                // loop through all of them using parallel threads
                #pragma omp parallel for
                for(ULLI lower = 0; lower < upper; lower += delta) {
                    // upper bound for the current thread
                    ULLI next = lower + delta;
                    // crack
                    std::vector<PasswordEntry> currSolved = BruteForcer(lower, (next-1), d).crack(unsolved);
                    // update the global list
                    mutex.lock();
                    solved.insert(solved.end(), currSolved.begin(), currSolved.end());
                    for(int p=0; p<currSolved.size(); p++) {
                        unsolved.erase(currSolved[p].hash);
                    }
                    mutex.unlock();
                }
            }
        }

        printf("Wall clock time taken to bruteforce \t\t\t= %f sec \n", getTimeElapsed());

		// print results to a file
		std::pair<std::string, std::string> pwFilePathParts = splitFilePath(argv[1]);
		std::ofstream solvedOutStream(pwFilePathParts.first + "_results" + pwFilePathParts.second);
        for (int x =0; x<solved.size(); x++) {
			// output the hex,pw,timetaken
			solvedOutStream << solved[x].hashHexString << "," << solved[x].password << "," << solved[x].timeTaken << std::endl;
		}

		return EXIT_SUCCESS;
	}

    // if fail
	displayMenu();
}
