#pragma once

#include <vector>
#include <set>
#include <string>
#include "md5.h"
#include "ULLI.h"

// function to meature the time taken
double getTimeElapsed();

// class to represent the hashed number as bits! This will make the program run uber fast
// we create different representations for them to make fast comparisons
union Hash {
    unsigned int uIntBin[4];
    unsigned long uLongBin[2];
    unsigned __int128 uLLLBin;
    
    // default
    Hash(){}
    // create a hash from a string
    Hash(const std::string& hexString);
    // convert from bits to hexstring
    const std::string getHexString() const;
    // Pass it on!
    bool operator<(const Hash &rhs) const
    {
        return uLLLBin < rhs.uLLLBin;
    }
    // Pass the operator on to 128 bits comparizon
    friend inline bool operator==(const Hash& lhs, const Hash& rhs){
        return lhs.uLLLBin == rhs.uLLLBin;
    }
};

// A struct to represent a password that is cracked
struct PasswordEntry {
    std::string hashHexString;
    std::string password;
    Hash hash;
    double timeTaken;
    // initialize the password entry
    PasswordEntry(const Hash& hash, const std::string& password) : hash(hash), 
    hashHexString(hash.getHexString()), password(password), timeTaken(getTimeElapsed()){}
};

// bruteforcer bruteforces between a certain range of numbers
class BruteForcer
{
public:
	// Characterize a single password entry in the file
    BruteForcer(const ULLI& lower, const ULLI& upper, unsigned int numDigits);
	// this executes this job of hashing, callable by tbb
	std::vector<PasswordEntry> crack(const std::set<Hash> unsolved);
private:
	// both lower and upper are inclusive
	const ULLI lower;
	const ULLI upper;
    unsigned int numDigits;
    // A temp so as to reuse the same over and over
    Hash tempHash;
	// Try an target over all the hashes, and test it across and see if it solves anything
	void inline tryCombination(std::vector<PasswordEntry>& solved, const std::set<Hash>& unsolved, const std::string& target);
};
