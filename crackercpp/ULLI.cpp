#include "ULLI.h"

// convertions between index and the char value
// we exploit the sequence of ASCII characters so as to prevent the lookup
inline char ctoi(char c) {
    return c - MIN;
}

inline char itoc(char i) {
    return i + MIN;
}

// we convert the number to a string 
std::string toString(ULLI d, unsigned int padding)
{
    // padd it with the appropriate padding
    std::string toReturn( padding, MIN);
    int p=0;
    ULLI newD;
    // until d is 0
    while( d )
    {
        newD = d / BASE;
        toReturn[p] = itoc(d - (BASE * newD));
        p++;
        d = newD;
    }
    return toReturn;
}

// a lookup for the powers of the base
ULLI lPow[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
