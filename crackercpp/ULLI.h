#pragma once

#include <string>

// We represent the combinations in a unsigned long long which is 64 bits to make calculations very fast
typedef unsigned long long int ULLI;

// keyset to use to crack the passwords
const char* const KEY = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
const long long BASE = 95;
const char MAX = 126;
const char MIN = 32;

extern ULLI lPow[];

extern std::string toString(ULLI d, unsigned int padding);
