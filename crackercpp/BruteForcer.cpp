#include "BruteForcer.h"

Hash::Hash(const std::string& hexString) {
    // Stores to binaries in accordance to MD5 hashing conventions
    uLongBin[0] = __builtin_bswap64(std::stoul(hexString.substr(0, 16), nullptr, 16));
    uLongBin[1] = __builtin_bswap64(std::stoul(hexString.substr(16, 32), nullptr, 16));
}
const std::string Hash::getHexString() const {
    // reverse the process of string -> bin
    union{
        unsigned long uLongBin[2];
        unsigned __int128 uLLLBin;
    } temp;
    temp.uLongBin[0] = __builtin_bswap64(uLongBin[1]);
    temp.uLongBin[1] = __builtin_bswap64(uLongBin[0]);
    static const char* digits = "0123456789abcdef";
    // start from the back(LSB)
    std::string rc(32,'0');
    for (int i=31; i>=0; --i){
        rc[i] = digits[(temp.uLLLBin) & 0x0f];
        temp.uLLLBin>>=4;
    }
    return rc;
}

BruteForcer::BruteForcer(const ULLI& lower, const ULLI& upper, unsigned int numDigits)
	: lower(lower), upper(upper), numDigits(numDigits)
{
}

void inline BruteForcer::tryCombination(std::vector<PasswordEntry>& solved, const std::set<Hash>& unsolved, const std::string& target) {
    
    // MD5 hash the string
    md5_hash((const unsigned char *)target.c_str(), target.size(), (unsigned int *) tempHash.uIntBin);
    
    // check if it exists before
    auto ite = unsolved.find(tempHash);
    if (ite != unsolved.end()) {
        // stores it
        PasswordEntry p(*ite, target);
        solved.emplace_back(p);
        // Display on screen as required
        printf("Cracked %s -> %s in %f seconds.\n", p.hashHexString.c_str() , p.password.c_str() , p.timeTaken);
    }
}

std::vector<PasswordEntry> BruteForcer::crack(const std::set<Hash> unsolved)
{
    std::vector<PasswordEntry> solved;
    solved.reserve(unsolved.size());
	// we try from lower to upper, both inclusive
	ULLI x = lower;
	while (x <= upper) {
		tryCombination(solved, unsolved, toString(x, numDigits));
		++x;
	}
    return solved;
}
