ITP125
======
There are 2 variants of the cracker in this repository: python and cpp

The cpp variant is in `crackercpp` while the python variant is in `passwordCracker.py`. Additionally, `pwHashes.txt` is the file containing all the hashes while `pwHashes_results.txt` contains the results of cracking the hashes. `Final Project.docx` contains the answers to the questions asked.

The python program is pretty much self-explainatory, with comments in the python source code.

The instructions for the cpp program are given below:

Required libraries
------------------
Please ensure that you have the following installed:

1. g++ 4.8 and above, with support for openmp
2. make

Software was tested on a linux machine and on a mac. Mac comupters need a version of gcc that has openmp, the default copy that comes with xcode does not come with openmp enabled.

Compiling the source code
-------------------------
In the crackercpp folder, execute the following in a linux shell.

```bash
$ make
```

Running the program
-------------------
In the crackercpp folder, execute:

```bash
$ ./cracker ../pwHashes.txt 4
```

That would run the cracker against the hash file, cracking for 1-4 length passwords.
