from itertools import product
from hashlib import md5
from time import time

print ((lambda s,l,c='!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~',t=time() : [(time()-t, ''.join(perm)) for n in range(1,l+1) for perm in product(c, repeat = n) if (lambda e=md5(''.join(perm).encode('utf-8')).hexdigest() : (e in s) and (s.discard(e) or True)) () ]) (set([ ea.strip().lower() for ea in (lambda x=open("pwHashes.txt") : (lambda u,v: u)(x.readlines(), x.close()))() ]), 3))


"""
Note: the parameter used is for 3 digit passwords. You can tweak it to be n digits. It is set to 3 for proof of concept. The python version runs too slowly for analysis. We use the cpp version for analysis instead.

Comments:

Ensures that the file is read in properly with the file being closed later  after reading
	(lambda x=open("pwHashes.txt") : (lambda u,v: u)(x.readlines(), x.close()))()

Create the set from the file
	set([ ea.strip().lower() for ea in (lambda x=open("pwHashes.txt") : (lambda u,v: u)(x.readlines(), x.close()))() ]),

Keyset used:
	'!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'

Used to keep track of when the program started:
	t=time()

Used to calculate time elasped and the computed password:
	[(time()-t, ''.join(perm))

Cracker will run from length 1 to l:
	for n in range(1,l+1)

Some clever code to delete from the set and return true
	(lambda e=md5(''.join(perm).encode('utf-8')).hexdigest() : (e in s) and (s.discard(e) or True)) () 

Running through all the combinations and deleting from the set if it exist
	for perm in product(c, repeat = n) if (lambda e=md5(''.join(perm).encode('utf-8')).hexdigest() : (e in s) and (s.discard(e) or True)) () 
"""
